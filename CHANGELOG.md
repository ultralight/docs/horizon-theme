# Changelog

Horizon will be diverging from the original Adidoks theme.

March 2024:
- Update docs theming for greater reusability
- Clone original theme