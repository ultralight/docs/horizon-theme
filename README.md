# Horizon Theme

Horizon started as AdiDoks. AdiDoks is a modern documentation theme, which is a port of the Hugo
theme [Doks](https://github.com/h-enk/doks) for Zola.

We're customizing it for our own needs.